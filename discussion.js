// CRUD

// Inserting Documents (Create)

/* Syntax: 
	db.collection.insertOne({object});
   Javascript Syntax:
   	object.object.method({object});	
*/
db.users.insertOne({
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "09196543210",
                "email": "janedoe@mail.com"
            },
        "courses": ["CSS", "Javascript", "Python"],
        "department": "none"    
    });


// Insert Many
/* Syntax:
	db.collectionName.insertMany([{objectA},{objectB}]);

*/
db.users.insertMany([{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "09179876543",
                "email": "stephenhawking@mail.com"
            },
        "courses": ["React", "PHP", "Python"],
        "department": "none"    
},
{
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
                "phone": "09061234567",
                "email": "neilarmstrong@mail.com"
            },
        "courses": ["React", "Laravel", "SASS"],
        "department": "none"    
}
]);

// Finding documents (Read) Operation
/* Find Syntax:
	db.collectionName.find();
	db.collectionName.find({field: value});
*/
db.users.find();

db.users.find({"lastName": "Doe"});

db.users.find({"lastName": "Doe", "age": 25}).pretty();